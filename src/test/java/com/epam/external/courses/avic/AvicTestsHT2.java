package com.epam.external.courses.avic;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

public class AvicTestsHT2 extends TestRunner {

    @Test(priority = 1)
    public void checkTopLeftPhoneNumber() {
        logger.info("start test checkTopLeftPhoneNumber()");
        String expected = "0-800-307-900";
        By phone_number = By.cssSelector(".header-top__item a[href*='0800307900']");
        String actual = driver.findElement(phone_number).getText();
        logger.info("get phone number: " + actual);
        assertEquals(actual, expected, "the actual phone number didn't match the number 0800307900");
    }

    @Test(priority = 2)
    public void checkLoginButton() {
        logger.info("start test checkLoginButton()");
        String expectedURL = "avic.ua/sign-in";
        By loginButton = By.cssSelector(".balance-btn+a[href*='sign-in']");
        logger.info("move to login form");
        driver.findElement(loginButton).click();
        String actual = driver.getCurrentUrl();
        logger.info("get URL address: " + actual);
        assertTrue(actual.contains(expectedURL), "the actual URL didn't match the avic.ua/sign-in");
    }

    @Test(priority = 3)
    public void checkRegistrationButton() {
        logger.info("start test checkRegistrationButton()");
        String expectedURL = "avic.ua/sign-up";
        By loginButton = By.cssSelector(".balance-btn+a[href*='sign-in']");
        By registrationButton = By.cssSelector(".container-right a[href*='sign-up']");
        logger.info("move to login form");
        driver.findElement(loginButton).click();
        logger.info("move to registration form");
        driver.findElement(registrationButton).click();
        String actual = driver.getCurrentUrl();
        logger.info("get URL address: " + actual);
        assertTrue(actual.contains(expectedURL), "the actual URL didn't match the avic.ua/sign-up");
    }

    @Test(priority = 4)
    public void checkSendMessageButton() {
        logger.info("start test checkSendMessageButton()");
        String expected = "Отправить сообщение";
        ExplicitWaitUtil wait = new ExplicitWaitUtil(driver);
        By sendMessageBottomButton = By.xpath(
                "//div[@class='footer__nav']//div[contains(@class, 'mobile-hidden')]/button[contains(@class, 'addMessage_btn')]");
        By formName = By.cssSelector("#js_addMessage>.modal-top>.ttl");
        logger.info("move to send message form");
        driver.findElement(sendMessageBottomButton).click();
        wait.visibilityOfElementLocated(formName);
        String actual = driver.findElement(formName).getText();
        logger.info("get message form name: " + actual);
        assertEquals(actual, expected, "the actual message form name didn't match the name 'Отправить сообщение'");
    }
}
